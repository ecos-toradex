Introduction
============

This is a compilation and usage manual for the port of the eCos :abbr:`RTOS (real-time operating system)` for the `Toradex Colibri VF61 Freescale Vybrid Computer on Module <http://developer.toradex.com/product-selector/colibri-vf61>`_.
It gives a brief overview on how to get the port, compile it and run an example program on the module using a Linux host.

The eCos port is targeted for the Cortex-M core of the heterogeneous Vybrid CPU, to provide a robust way to drive a real-time control setup, and is best combined with Linux or similar OS running on the Colibri VF61 Cortex-A core for handling non-critical outside communication, user interfaces etc.

ECos
----

ECos is a configurable RTOS intended for use in embedded applications.
The documentation for eCos 3.0, which is the most recent version of the system as well as the one ported to Colibri VF61, can be found at http://ecos.sourceware.org/docs-3.0/.

A comprehensive PDF `eCos Reference Guide`_ is also available from the eCos website.

.. _eCos Reference Guide: http://ecos.sourceware.org/docs-3.0/pdf/ecos-3.0-ref-a4.pdf

Licence
-------

*(based on the* `eCos licence overview <http://ecos.sourceware.org/license-overview.html>`_\ *)*

ECos is released under a modified version of the well known `GNU General Public License (GPL) <http://www.gnu.org/copyleft/gpl.html>`_. The eCos license is officially recognised as a GPL-compatible Free Software License. An **exception clause** has been added which limits the circumstances in which the license applies to other code when used in conjunction with eCos. The exception clause is as follows:

   As a special exception, if other files instantiate templates or use macros or inline functions from this file, or you compile this file and link it with other works to produce a work based on this file, this file does not by itself cause the resulting work to be covered by the GNU General Public License. However the source code for this file must still be made available in accordance with section (3) of the GNU General Public License.

   This exception does not invalidate any other reasons why a work based on this file might be covered by the GNU General Public License.

**The license does not require users to release the source code of any** *applications* **that are developed with eCos.**

Supported features
------------------

This eCos port provides the following software packages specific for Toradex Colibri Vybrid VF61 Vybrid module:

* HAL package
* debug UART driver
* serial port driver
* Flex Timer Module
* GPIO handling

Also, the port has been verified to work with the standard eCos POSIX an µITRON compatibility layers.
See :doc:`appendix-b` for more information on this.

Version information
-------------------

.. csv-table::
   :header: Author,Content,Date,Version

   Peter Katarzynski,Draft version,2014-03-21,0.1.0
   Michael Gielda,Revamp,2014-03-27,0.2.0
   Michael Gielda,Prerequisites & compiling sample programs,2014-03-27,0.3.0
   Michael Gielda,Running programs,2014-03-28,0.3.1
   Michael Gielda,Further updates,2014-04-02,0.3.2
   Michael Gielda,Further updates & Appendix B,2014-04-07,0.4.0
   Michael Gielda,Cleanup & division into files,2014-04-08,0.5.0
   Michael Gielda,Simplified instructions & making tests,2014-04-11,0.5.1
   Michael Gielda,Added POSIX compatibility description,2014-04-11,0.5.2

