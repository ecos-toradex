Running an eCos application on Colibri VF61
===========================================

This chapter will explain how to run the eCos application on the Colibri VF61 Cortex-M core from the Cortex-A core in two ways: either from Linux using ``MQXBboot`` or from U-Boot using TFTP.

Vybrid has three available memory regions:

* OCRAM - 256KB - Default.
* DRAM - 10MB - Available, but needs limiting Linux RAM memory.
* TCML - 32KB - Small. Not recommended.

.. note::

   Out of the 16MB of the DRAM memory in the CPU, 6MB was reserved for enabling the passing of large data blocks between Cortex-A and Cortex-M.
   This setting can be changed in the ``mlt_vybrid_ext_dram.ldi`` file located in ``ecos/packages/hal/cortexm/vybrid/col_vf61/current/include/pkgconf/``, by modifying the ``DRAM LENGTH`` and ``hal_startup_stack`` values (currently ``0x9FFFF0``).

U-Boot over TFTP
----------------

Set up TFTP on your host machine and put the binary to be loaded (for example, ``hello.bin``) there. Make sure your host machine is connected to the network and you know its IP address.

.. _u-boot:

Configuring U-Boot
~~~~~~~~~~~~~~~~~~

.. note::

   The memory restriction is mandatory only if DRAM is used to run eCos. See :ref:`memory` for details.

Connect the module to the network with an Ethernet cable, power it on.
Enter U-Boot and then use the following command sequence:

.. code-block:: bash
  
   dhcp                         # set module IP address
   set memargs mem=240M         # restrict Linux memory space
   set serverip xxx.xxx.xxx.xxx # set TFTP server address
   save                         # save the configuration

Running from U-Boot
~~~~~~~~~~~~~~~~~~~

The application can then be run over TFTP with the ``tftp`` command.
The two other ``mw`` commands will set the entry point and turn on the clocks, respectively.
OCRAM is recommended as default, but you may refer to :ref:`memory` for information on what memory to use and how to get eCos to run from it.

.. topic:: OCRAM (default)

   .. code-block:: bash

      tftp 0x3f000400 hello.bin
      mw.l 0x4006e028 0x1f000411
      mw.l 0x4006b08c 0x00015a5a

.. topic:: DRAM

   .. code-block:: bash

      tftp 0x8f000400 hello.bin
      mw.l 0x4006e028 0x0f000411
      mw.l 0x4006b08c 0x00015a5a

.. topic:: TCML

   .. code-block:: bash
   
      tftp 0x1f800400 hello.bin
      mw.l 0x4006e028 0x1f800411
      mw.l 0x4006b08c 0x00015a5a

Linux over MQXBoot
------------------

Requirements
~~~~~~~~~~~~

The following have to be present on the Cortex-A Linux (apart from the eCos binary) to make this method possible:

* ``mcc.ko`` kernel module
* ``mqxboot`` binary

Running from MQXBoot
~~~~~~~~~~~~~~~~~~~~

The command to run the binary depends on the memory we want to use.
OCRAM is recommended as default, but you may refer to :ref:`memory` for information on what memory to use and how to get eCos to run from it.

.. topic:: Loading to OCRAM (default)

   .. code-block:: bash

      mqxboot hello.bin 0x3f000400 0x1f000411

.. topic:: Loading to DRAM

   .. code-block:: bash

      mqxboot hello.bin 0x8f000400 0x0f000411

.. topic:: Loading to TCML

   .. code-block:: bash

      mqxboot hello.bin 0x1f800400 0x1f800411

