Appendix A: custom eCos configuration
=====================================

ECos is called a *configurable* system for a reason: it contains a powerful infrastructure for choosing what system components and abstractions are included and how they are configured.

This Appendix will describe briefly how to deal with ``.ecc`` files for the Colibri VF61.

Background
----------

The main tool used for building the eCos operating system is :program:`ecosconfig` (see :ref:`prerequisites`).
The source tree of eCos, called **eCos repository** (like for example the source code tree provided in this release) is not built directly but instead first trimmed down and configured to suit the needs of a specific user and platform using :program:`ecosconfig`.
This static pick-and-build procedure allows the user to exclude these elements of the system which are not necessary, thus reducing the memory footprint.
This mechanism also enables easy configuration of system-wide variables and driver specific features.

What exactly can be included, excluded or configured is determined by the contents of :file:`.cdl` files residing side by side with all source files in the eCos repository (usually in the :file:`cdl` directory on the same level as the :file:`src` directory of a given package, like a driver for a particular interface).

:program:`configtool`
---------------------

:program:`configtool` is a GUI front-end to :program:`ecosconfig` to facilitate the creation of eCos configuration files.
It also may be downloaded from `eCosCentric <http://www.ecoscentric.com/snapshots/configtool-100305.bz2>`_.

.. warning::

   :program:`configtool` (just like :program:`ecosconfig`) is a 32bit application, thus if you are using a 64bit OS you have to provide 32bit run-time libraries for compatibility. In a Debian-based Linux distributions these could be installed using the command ``sudo apt-get install ia32-libs``.

.. warning::

   The official :program:`configtool` version given above was found not to work on Arch Linux and Fedora Linux distributions. In case of problems, you can try `a version from another source <https://github.com/flyskywhy/ecos-tools/raw/master/bin/configtool>`_ or `compile your own version <http://www.ecoscentric.com/devzone/configtool.shtml#linux>`_.

Templates
---------

While creating a new ``.ecc`` file it is easier to also use a predefined template representing common use scenarios, such as **posix** which represents a system which has all the necessary packages to run typical POSIX programs or **redboot** which understandably is used to build a binary of RedBoot, the eCos bootloader.

In order to select a template to base upon, use :menuselection:`build --> templates`.

.. warning::

   Remember that the templates are just general scenarios, which may contain settings incompatible with the desired ones (baudrates, console mangling, debug console choice, presence of RedBoot ROM monitor). It is necessary to tweak them according to your needs.

Preparing an ``.ecc`` file using :program:`configtool`
----------------------------------------------------------

Launch :program:`configtool`. 

Select :menuselection:`build --> repository` specify the path to eCos repository (the ``packages`` directory).
Select the :menuselection:`build --> template` option and choose the :guilabel:`Toradex Colibri VF61` as your hardware platform with default set of packages.
Click :guilabel:`continue` to proceed.

When the default set of packages is used for the platform, the associated ``.ecc`` file can already be prepared.
Save it in a directory accessible by your build script and **remember to point to it in** :ref:`the kernel build script <build-kernel>`.

Other packages can be added from :menuselection:`build --> packages`, bear in mind that you may need to alter the chosen packages and options to satisfy some ``.cdl`` constraints.

.. _memory:

Startup memory choice
---------------------

There are three memories from which eCos software may be launched in Colibri VF61.
This is determined in the ``.ecc`` file the eCos kernel was based on.

By default the software is prepared to be launched from OnChip RAM (OCRAM).
Alternatively DRAM memory may be used for booting.
In this approach however the DDR memory block assigned to Linux must be limited to prevent Linux from accessing the memory region already occupied by eCos.
This may be achieved by altering the boot arguments from U-Boot as described in the section entitled :ref:`u-boot`.

The OCRAM and DDR scenarios are recommended; alternatively you may also try to run eCos from TCML, but this method was not tested.
Besides, TCML offers a limited amount of memory which may be insufficient for many eCos applications.

To modify the startup memory scenario in the eCos kernel, :program:`configtool` can be used.
The appropriate menu option is:

:menuselection:`eCos HAL --> Cortex-M Architecture --> Freescale Vybrid Cortex-M4 Variant --> Toradex Colibri VF61 Platform --> Startup type`

The associated parameters are:

* CYG_HAL_STARTUP_PLF (ByVariant / DRAM)
* CYG_HAL_STARTUP_VAR  (OCRAM / TCML) 

