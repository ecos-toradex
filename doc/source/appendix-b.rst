Appendix B: POSIX and µITRON compatibility
==========================================

By default, eCos allows the user to enable POSIX or µITRON compatibility, which may be beneficial for many applications.
This Appendix explains briefly how to use them with this eCos port.

For more information refer to the `eCos Reference Guide`_, Chapters XIII and XIV.

.. _eCos Reference Guide: http://ecos.sourceware.org/docs-3.0/pdf/ecos-3.0-ref-a4.pdf

POSIX
-----

:abbr:`POSIX (Portable Operating System Interface)`, is a well-known family of OS standards.
POSIX defines the primitives, nomenclature and API which makes it easier to provide software compliance between operating systems.

This is especially useful with regard to programming heterogeneous devices like the Colibri VF61,
where (especially with the POSIX compatibility layer enabled),
the programming style for eCos applications dedicated for the Cortex-M4 core can vastly resemble that of Linux programs running on the Cortex-A5 core,
lowering the entry barrier for programmers.

To activate this package, use either :program:`ecosconfig` (``ecosconfig add CYGPKG_POSIX``) or :program:`configtool`:

#. Choose :menuselection:`Build --> Packages`
#. Select :guilabel:`POSIX compatibility layer` and click :guilabel:`Add >>`, then :guilabel:`OK`
#. A new package, ``POSIX compatibility layer`` should appear in the package list - save your ``.ecc`` file and exit.

A sample application, verified to work as expected using the port can be found inside the port code, in the subdirectory :file:`compat/posix/current/tests/pthread1.c`.

µITRON
------

µITRON is a the name of an Japanese open standard for RTOS, originally undertaken in 1984 under the guidance of Ken Sakamura.
eCos supports the µITRON version 3.02 specification, with complete "Standard functionality" (level S), plus many "Extended" (level E) functions.

More about ITRON and µITRON can be read in the following sources: 

* `introduction to ITRON project <http://www.ertl.jp/ITRON/panph98/panph98-e.html>`_
* `µITRON3.0 specification <http://www.ertl.jp/ITRON/spec-e.html#ITRON3>`_
* Dr. Sakamura’s book: uITRON 3.0, An Open and Portable Real Time Operating System for Embedded Systems

Since eCos was designed with the µITRON guidelines in mind, it is not strictly necessary to "activate" this compatibility layer, as an eCos application may fulfill the standard anyway.
However, eCos provides a package named ``CYKPKG_UITRON`` setting some additional constraints, and adding it to the kernel is recommended for applications meant to be µITRON compliant.
To activate this package, use either :program:`ecosconfig` (``ecosconfig add CYGPKG_UITRON``) or :program:`configtool`:

#. Choose :menuselection:`Build --> Packages`
#. Select :guilabel:`uITRON compatibility` and click :guilabel:`Add >>`, then :guilabel:`OK`
#. A new package, ``uITRON compatibility layer`` should appear in the package list - save your ``.ecc`` file and exit. 

A sample application, verified to work as expected using the port can be found inside the port code, in the subdirectory :file:`compat/uitron/current/tests/test3.c`
