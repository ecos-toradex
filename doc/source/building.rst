Building the eCos kernel and applications
=========================================

This chapter describes how to build an eCos kernel and compile eCos applications for Colibri VF61.

The build process was tested on the Gentoo, Debian, Ubuntu and Mint Linux distributions.
The procedures described here should also work on other systems, but if you find any way to improve this manual with respect to tested platforms, please e-mail us at contact@antmicro.com.

.. note::

   The code blocks below, when copy-pasted to a Linux terminal, should all work, provided they were called from the same directory.
   To avoid confusion, it is best to call them from within a new, empty directory, e.g.:

   .. code-block:: bash

      mkdir ~/ecos-from-scratch
      cd ~/ecos-from-scratch

.. _prerequisites:

Prerequisites
-------------

Toolchain
~~~~~~~~~

This port of eCos was prepared using a pre-built standard eCos toolchain, which can be obtained e.g. from the `GWDG FTP server <ftp://ftp.gwdg.de/pub/misc/sources.redhat.com/ecos/gnutools/i386linux/test/>`_.

.. code-block:: bash
   
   wget ftp://ftp.gwdg.de/pub/misc/sources.redhat.com/ecos/gnutools/i386linux/test/\
   ecos-gnutools-arm-eabi-20120623.i386linux.tar.bz2
   tar xjvf ecos-gnutools-arm-eabi-20120623.i386linux.tar.bz2 
	
Alternatively it is possible to compile eCos software using self-built toolchains as described `on the eCos website <http://ecos.sourceware.org/getstart.html>`_.

To compile eCos and eCos applications, the toolchain's :file:`bin` directory has to be included in the PATH variable.
The proper availability of the toolchain can be checked by finding out if ``arm-eabi-gcc`` is available from the shell.

:program:`ecosconfig`
~~~~~~~~~~~~~~~~~~~~~

The :program:`ecosconfig` tool, available from the `eCosCentric website <http://www.ecoscentric.com/snapshots/ecosconfig-100305.bz2>`_, is used to generate the build tree from the main repository and is a mandatory requirement.
:program:`ecosconfig` requires the tcl compiler to work. 

.. topic:: Installing tcl on Debian-based distributions

   .. code-block:: bash

      sudo apt-get install tcl8.5 # use sudo emerge dev-lang/tcl for Gentoo

Now you can download and use :program:`ecosconfig`. You also need to make :program:`ecosconfig` executable after downloading and extracting it from the archive. It is also a good idea to make it available system-wide by moving it to ``/usr/local/bin``.

.. topic:: Installing ecosconfig

   .. code-block:: bash

      wget http://www.ecoscentric.com/snapshots/ecosconfig-100305.bz2
      bunzip2 ecosconfig-100305.bz2
      chmod +x ecosconfig-100305
      sudo mv ecosconfig-100305 /usr/local/bin/ecosconfig

.. warning::

   :program:`ecosconfig` is a 32bit application, thus if you are using a 64bit OS you have to provide 32bit run-time libraries for compatibility. In a Debian-based Linux distributions these could be installed using the command ``sudo apt-get install ia32-libs``.

.. note::

   The output of :program:`ecosconfig` are :abbr:`.ecc (eCos Configuration)` files which are in essence ``tcl`` scripts storing all the information on what elements will be included in the system image and how they will be configured.
   
.. note::
   
   A `handbook on ecosconfig <http://ecos.sourceware.org/docs-3.0/user-guide/using-ecosconfig-on-linux.html>`_ exists to help in the manual creation of ``.ecc`` files.
   Also, if you want to create custom eCos configuration files, see :doc:`appendix-a`.

Source code
-----------

The source of the port can be downloaded by using the following command:

.. topic:: Downloading the Colibri VF61 eCos source

   .. code-block:: bash

      git clone https://github.com/mgielda/ecos-colibri-vf61.git
      
Preparing an ``.ecc`` file
--------------------------

The actual configuration of the eCos system is maintained and modified through :program:`ecosconfig`.
The following commands will prepare a sample ``.ecc`` file for a kernel with default settings.

.. topic:: Generating the kernel ecc file from scratch

   .. code-block:: bash

      export ECOS_REPOSITORY="$PWD/ecos-colibri-vf61/ecos/packages"
      # Create ecos.ecc file based on Colibri VF61 default template
      ecosconfig new col_vf61 default

You now have a ``ecos.ecc`` file that holds the default eCos configuration for Colibri VF61.
The file can be further edited manually with a text editor and/or :program:`ecosconfig` or graphically using :program:`configtool` (see :doc:`appendix-a`), but at this moment it is already enough to compile a sample eCos kernel.

.. _build-kernel:

Building the kernel
-------------------

The eCos kernel is built in two stages:

* first, a so-called *build tree* is generated from the eCos sources by :program:`ecosconfig`.
  The build tree is customized for your build as configured in the ``.ecc`` file used.
  It is best to generate the build tree in a separate directory (here ``build-tree``). 
* then, the source files are compiled

.. warning::

   When copy-pasting the following to the terminal, take care not to export the PATH variable multiple times.

.. topic:: Building the eCos kernel

   .. code-block:: bash

      export PATH="$PWD/gnutools/arm-eabi/bin:$PATH"
      export ECOS_REPOSITORY="$PWD/ecos-colibri-vf61/ecos/packages"

      mkdir -p build-tree
      rm -rf build-tree/*
      cd build-tree

      ecosconfig --config=$PWD/../ecos.ecc tree
      make
      cd ..

The resulting kernel files can be found in :file:`build-tree/install/lib`.

Application
-----------

With a compiled kernel files in the :file:`build-tree/install/lib` directory (see :ref:`build-kernel`), a user space eCos application can be compiled and linked to it.

A listing for a short sample application (taken from :file:`ecos-colibri-vf61/ecos/examples/hello.c`) is given below.

.. topic:: hello.c - sample application

   .. code-block:: c

      #include <stdio.h>

      int main(void)
      {
          printf("Hello, eCos world!\r\n");
          return 0;
      }

You can compile an eCos program with a procedure similar to the following listing (which you can save for reuse, for example as ``make.sh``):

.. warning::

   When copy-pasting the following to the terminal, take care not to export the PATH variable multiple times.

.. topic:: Building a user space application

   .. code-block:: bash

      export PATH="$PWD/gnutools/arm-eabi/bin:$PATH"

      # Set compiler options
      OPT="-Wall -Wpointer-arith -Wstrict-prototypes -Wundef  \
           -Wno-write-strings -mthumb -g -O2 -fdata-sections  \
           -ffunction-sections -fno-exceptions -nostdlib      \
           -mcpu=cortex-m4"

      # Set path to eCos kernel
      BTPATH="$PWD/build-tree"

      # Do compilation and link your application with kernel
      arm-eabi-gcc -g -I./ -g -I${BTPATH}/install/include hello.c \
                   -L${BTPATH}/install/lib -Ttarget.ld ${OPT}

      # Use objcopy to generate a binary
      arm-eabi-objcopy -O binary a.out hello.bin

Tests
-----

ECos is shipped with a test suite - in essence a set of simple programs checking various subsystems and interfaces - which can be easily compiled and run on the target.
These tests were used in the porting effort, and many (although not all) of them can be used as bases for user programs.

In order to compile tests, use the same procedure as described in :ref:`build-kernel`, but issue ``make tests`` instead of ``make`` at the end.

The resulting tests will reside in the ``build-tree/install/tests`` directory.
