ECos for Toradex Colibri VF61 Freescale Vybrid CoM
==================================================

This is a port of the eCos 3.0 RTOS (real-time operating system) for `Toradex's Colibri VF61 Freescale Vybrid Computer on Module <http://developer.toradex.com/product-selector/colibri-vf61>`_.

The eCos port is targeted for the Cortex-M core of the heterogeneous Vybrid CPU, to provide a robust way to drive a real-time control setup, and is best combined with Linux or similar OS running on the Colibri VF61 Cortex-A core for handling non-critical outside communication, user interfaces etc.

ECos
----

ECos is a configurable real-time operating system intended for use in embedded applications.
The documentation for eCos 3.0, which is the most recent version of the system as well as the one ported to Colibri VF61, can be found at http://ecos.sourceware.org/docs-3.0/.

Licence
-------

*(based on the* `eCos licence overview <http://ecos.sourceware.org/license-overview.html>`_\ *)*

ECos is released under a modified version of the well known `GNU General Public License (GPL) <http://www.gnu.org/copyleft/gpl.html>`_. The eCos license is officially recognised as a GPL-compatible Free Software License. An **exception clause** has been added which limits the circumstances in which the license applies to other code when used in conjunction with eCos. The exception clause is as follows:

   As a special exception, if other files instantiate templates or use macros or inline functions from this file, or you compile this file and link it with other works to produce a work based on this file, this file does not by itself cause the resulting work to be covered by the GNU General Public License. However the source code for this file must still be made available in accordance with section (3) of the GNU General Public License.

   This exception does not invalidate any other reasons why a work based on this file might be covered by the GNU General Public License.

**The license does not require users to release the source code of any** *applications* **that are developed with eCos.**

Supported features
------------------

This eCos port provides the following software packages specific for Toradex Colibri Vybrid VF61 Vybrid module:

* HAL package
* debug UART driver
* serial port driver
* Flex Timer Module
* GPIO handling

Documentation
-------------

The documentation for this port, residing in the ``doc`` directory, is written in ReStructuredText and is meant to be generated to HTML or PDF via Sphinx.

It is however possible to use GitHub ``.rst`` file rendering as a fallback:

* `Introduction <doc/source/introduction.rst>`_
* `Building the eCos kernel and applications <doc/source/building.rst>`_
* `Running an eCos application on Colibri VF61 <doc/source/running.rst>`_
* `Appendix A: custom eCos configuration <doc/source/appendix-a.rst>`_
* `Appendix B: POSIX and µITRON compatibility <doc/source/appendix-b.rst>`_
